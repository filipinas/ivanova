# Product Catalogue

This is an Android application example catalogue space template.

## Intro:

> [Contentful][1] is a content management platform for web applications, mobile apps and connected devices. It allows you to create, edit & manage content in the cloud and publish it anywhere via powerful API. Contentful offers tools for managing editorial teams and enabling cooperation between organizations.

## Screenshots

![Screenshots](screenshots/sc1.png) ![Screenshots](screenshots/sc2.png)

## Usage

- Create a space with the "Product Catalogue" space template on [Contentful][1]
- Clone this repo
- Enter your Delivery API credentials in the [Const.java][3] file
- \o/


[2]: LICENSE
[3]: app/src/main/java/catalogue/templates/contentful/lib/Const.java